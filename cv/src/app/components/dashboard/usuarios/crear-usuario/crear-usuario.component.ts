import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioDataI } from 'src/app/interface/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {
  idM!:number;
  adicionar:boolean=true;
  titulo ='Agenda una cita conmigo';
  form!:FormGroup;
  radio!:Date;
  // sexo: any[] = ['Masculino', 'Femenino'];
  constructor(private fb:FormBuilder,
              private _usuarioService: UsuarioService,
              private router:Router, 
              private _snackbar: MatSnackBar,
              private activeRouted: ActivatedRoute) {
    this.insertarUsuario();
    this.activeRouted.params.subscribe(params => {
      this.idM = params['id'];
      console.log(this.idM);
      
      this.form = this.fb.group({
        nombre: ['',[Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
        apellido: ['', [Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
        email: ['', [Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
        celular: ['', [Validators.required,Validators.pattern(/^[0-9\s]+$/)]],
        descripcion: ['', [Validators.required,Validators.pattern(/^[0-9a-zA-ZñÑ\s]+$/)]],
        fecha: ['', Validators.required]

      });
      localStorage.setItem('dateTime',this.form.value.fecha);

      this.radio =  JSON.parse(localStorage.getItem('dateTime') || '{}' );
      //this.radio = this.form.value.fecha;
        console.log(this.radio);
      
      if(this.idM.toString() !== 'nuevo'){

        const usuario = this._usuarioService.buscarUsuario(this.idM);
        //console.log(usuario);

        //Verifico que la longitud del objeto es cero
        if(Object.keys(usuario).length === 0){
         this.router.navigate(['/inicio/agenda']);
        }

        this.form.patchValue({
          nombre: usuario.nombres,
          apellido: usuario.apellidos,
          email: usuario.email,
          celular: usuario.celular,
          descripcion: usuario.descripcion,
          fecha: usuario.fecha
        });

        this.adicionar = false;
        this.titulo = 'Modificar Usuario';
      }
      
    });
   }

  ngOnInit(): void {
  }


  insertarUsuario(){
    this.form = this.fb.group({
      nombre: ['',[Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
      apellido: ['', [Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
      email: ['', [Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      celular: ['', [Validators.required,Validators.pattern(/^[0-9\s]+$/)]],
      descripcion: ['', [Validators.required,Validators.pattern(/^[0-9a-zA-ZñÑ\s]+$/)]],
      fecha: ['', Validators.required]
    });
  }

  agregarUsuario():void{
    if (!this.form.valid) {
      return;
    }
    
    const usuario = this._usuarioService.buscarUsuario(this.idM);
    // let fech = this.form.value.fecha as Date
    // console.log(fech);
    
      const user: UsuarioDataI ={
        id       :'',
        nombres   :this.form.value.nombre,
        apellidos :this.form.value.apellido,
        email :this.form.value.email,
        celular :this.form.value.celular,
        fecha :'',
        hora :'',
        descripcion :this.form.value.descripcion,
      }

    
    
    console.log(user);
    if (this.adicionar) {
      let date= new Date();

      //user.fecha = `${date.getDate() < 10? '0'+date.getDate(): date.getDate()}/${(date.getMonth()+1) < 10? '0'+(date.getMonth()+1): (date.getMonth()+1) }/${date.getFullYear()}`;
      user.fecha = this.form.value.fecha;
      user.hora = this.form.value.fecha;
      //user.hora = `${date.getHours() < 10? '0'+date.getHours(): date.getHours()}:${(date.getMinutes()+1) < 10? '0'+ date.getMinutes():date.getMinutes() }:${date.getSeconds() < 10? '0'+date.getSeconds(): date.getSeconds()}`;
      //nuevooooooooooooooooooooooooooooooooo
      localStorage.setItem('dateTime',this.form.value.fecha);

      
      user.id = this.makeRandomId();

      this._usuarioService.agregarUsuario(user);
      
      this.router.navigate(['/inicio/agenda']);

      this._snackbar.open('El mensaje fue agregado con exito', '', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });
    }else {
      user.id = usuario.id;
      user.fecha = usuario.fecha;
      user.hora = usuario.hora;
      this._usuarioService.modificarUsuario(user);

      this.router.navigate(['/inicio/agenda']);
  
      this._snackbar.open('El mensaje fue modificado con exito', '', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });
    }


  }


  Volver(): void {
    this.router.navigate(['/inicio/agenda']);
  }


  makeRandomId(length = 4) {
    let result = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    for (let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
   }
   return result;
}
  
}
